require 'test_helper'

class PagesTest < ActionDispatch::IntegrationTest
  def test_home_page
    get '/'
    assert_response :success
  end
  def test_about_page
    get '/about'
    assert_response :success
  end
  def test_contact_page
    get '/contact'
    assert_response :success
  end
end
