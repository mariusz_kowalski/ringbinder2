class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, limit: 32
      t.references :account, index: true

      t.timestamps
    end
    add_index :users, :name, unique: true
  end
end
