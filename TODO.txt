TODO
 pages (home, contact, about)
	 routes - integration tests
◯ users
	◯ model user
		User has an email and optional nick. User can login via email or nick.
		Email and nick can't be changed. Email and nick are unique.
		◯ create when account creates
		◯ delete when account deletes
	◕ model account
		 registration     |
		 delete account   |  God bless Devise!
		 change password  |
		 reset password   |
		◯ activate / deactivate - by admin
	 session controller
		 login    |
		 logout   |  Devise
◯ application layout
	◯ layout scaffold
	◯ header
	◯ menu
	◯ flash messages
	◯ content
	◯ sign in/ out/ up links
	◯ footer
◯ document model - controllers actions: index, new, create, show, edit, update, delete
	thing over about edit, update actions - maybe better actions will be publish, hide, publish_now
	model:
		title
		content -> content last version
		publish_date
		published
◯ content model - content of document
	◯ create, show only actions 

◯ Roles (very simple roles)
	Figure out how to realize roles...
	Roles are linear, it means that every role has assigned number and lower number role is in 
	subset of higher number role. For example if some action requires role number 3 user with role 2
	is also allowed to perform action.
	User has one role. Every action has 'required role' to perform. User can be owner of asset on which
	action is performed and in that case action can require different role.
